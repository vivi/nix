# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running `nixos-help`).

{ pkgs, lib, ... }:

{
  imports =
    [
      ./disks.nix
      ./hardware-configuration.nix
    ];
  
  nix = {
    settings = {
      allowed-users = [ "@wheel" ];
      auto-optimise-store = true;
      experimental-features = [ "nix-command" "flakes" ];
    };
  };

  boot = {
    extraModprobeConfig = ''
      options snd-intel-dspcfg dsp_driver=3
    '';
    initrd.systemd.enable = true;
    kernelPackages = pkgs.linuxPackages_latest;
    kernelParams = [ "ia32_emulation=false" ];
    #kernelModules = lib.mkBefore [ "intel-pmc-mux" "cros-ec-typec" ];
    #blacklistedKernelModules = [ "" ];
    loader = {
      systemd-boot.enable = true;
      systemd-boot.memtest86.enable = true;
      efi.canTouchEfiVariables = true;
    };
  };


  zramSwap = { 
    enable = true;
    memoryMax = (256 * 1048576);
  };

  time.timeZone = "America/Chicago";

  environment.persistence."/persist" = {
    files = [
      "/etc/machine-id"
      "/etc/ssh/ssh_host_rsa_key"
      "/etc/ssh/ssh_host_rsa_key.pub"
      "/etc/ssh/ssh_host_ed25519_key"
      "/etc/ssh/ssh_host_ed25519_key.pub"
    ];     
    directories = [ 
      { directory="/var/lib/iwd"; mode="0700"; }
      #{ directory="/var/lib/tailscale"; mode="0700"; }
    ];
    users.user = {
      directories = [
        { directory=".config/mpv"; mode="0700"; }
        { directory=".config/sway"; mode="0700"; }
        { directory=".config/waybar"; mode="0700"; }
        { directory=".local/bin"; mode="0700"; }
        { directory=".mozilla"; mode="0700"; }
        { directory=".passage"; mode="0700"; }
        { directory=".ssh"; mode="0700"; }
        { directory="keep"; mode="0700"; }
      ];
    };
  };

  networking = {
    hostName = "puppy";
    useDHCP = false;
    wireless.iwd.enable = true;
    firewall.enable = false;
    nftables.enable = true;
    networkmanager.enable = false;
  };

  systemd.network = {
    enable = true;
    wait-online.anyInterface = true;
    networks = {
      "10-wired" = {
        matchConfig.Name = "en*";
        networkConfig.DHCP = "yes";
        dhcpV4Config.UseDNS = false;
        dhcpV6Config.UseDNS = false;
        ipv6AcceptRAConfig.UseDNS = false;
      };
      "20-wireless" = {
        matchConfig.Name = "wlan0";
        networkConfig.DHCP = "yes";
        dhcpV4Config.UseDNS = false;
        dhcpV6Config.UseDNS = false;
        ipv6AcceptRAConfig.UseDNS = false;
      };
    };
  };

  users = {
    mutableUsers = false;
    users.user = {
      initialHashedPassword = "$y$j9T$2EblDvSSqdo9.OzBSVgI10$YJNxraJBOKji.KiAY857Sm6FWE.Phi.mxGkJtpK.a75";
      isNormalUser = true;
      extraGroups = [ "adbusers" "wheel" ];
      packages = with pkgs; [
        age
        age-plugin-yubikey
        android-tools
        bc
        cbmem
        ffmpeg
        flyctl
        fw-ectool
        lftp
        mpv
        nil
        nixpkgs-fmt
        nix-prefetch
        nmap
        passage
        pciutils
        pulsemixer
        scrcpy
        streamlink
        vlc
        waybar
        whois
        wl-clipboard
        yt-dlp
        xdg-utils
      ];
      openssh.authorizedKeys.keys = [
      "sk-ssh-ed25519@openssh.com AAAAGnNrLXNzaC1lZDI1NTE5QG9wZW5zc2guY29tAAAAIC/az8vkq+0ap64Yb6XbioN50qEzr66irdOtF2RfQPx/AAAACHNzaDp1c2Vy"
      "sk-ssh-ed25519@openssh.com AAAAGnNrLXNzaC1lZDI1NTE5QG9wZW5zc2guY29tAAAAIDZMqzSOWu7ypYL6+KdIirn1ItrJy525Ghaspy6XZYMsAAAACHNzaDp1c2Vy"
      ];
    };
  };

    fonts.packages = with pkgs; [
    font-awesome
    noto-fonts
    noto-fonts-cjk
    noto-fonts-emoji
  ];


  nixpkgs.overlays = with pkgs; [ ( final: prev:
  {
    keyd = prev.keyd.overrideAttrs (old: {
      src = (fetchFromGitHub {
          owner = "rvaiya";
          repo = "keyd";
          rev = "2c288d0e998c4f0c59cd748aa0630f6be5330af5";
          hash = "sha256-D7nf1SWT23Ytw+gHeEd5bXqc021JI2GN05WJNW78mQU=";
      });
      });
#     alsa-ucm-conf = prev.alsa-ucm-conf.overrideAttrs (old: {
#      wttsrc = (fetchFromGitHub {
#        owner = "WeirdTreeThing";
#        repo = "chromebook-ucm-conf";
#        rev = "6d8835f89c7c7e5fac7b6aaea0e36a6b1d1e4be5";
#        hash = "sha256-XQ/kRN1xHEAcykbKpE26mNZXwqEbkh++ZuJeCAiu9ho=";
#      });
#      installPhase = ''
#        runHook preInstall
#  
#        mkdir -p $out/share/alsa
#        cp -r ucm ucm2 $out/share/alsa
#  
#        mkdir -p $out/share/alsa/ucm2/conf.d
#        cp -r $wttsrc/common/pcm $wttsrc/adl/* $out/share/alsa/ucm2/conf.d
#  
#        runHook postInstall
#        '';
#      });
  })];
  environment.sessionVariables = {
    ALSA_CONFIG_UCM2 = "${pkgs.alsa-ucm-conf}/share/alsa/ucm2";
  };

  environment.systemPackages = with pkgs; [
    (callPackage ./chromebook-ucm-conf.nix {})
    #alsa-ucm-conf
    brightnessctl
    dig
    dmidecode
    git
    keyd
    mosh
    ncdu
    pciutils
    strace
    tmux
    usbutils
    unzip
    util-linux
  ];

  services = {
    avahi = {
      enable = false;
      nssmdns4 = true;
      openFirewall = true;
      publish.enable = false;
    };
    #fwupd.enable = true;
    #hardware.bolt.enable = true;
    keyd = {
      enable = true;
      keyboards.internal = {
        ids = [ "0001:0001" ];
        settings = {
          main = {
            back = "f1";
            refresh = "f2";
            zoom = "f3";
            scale = "f4";
            sysrq = "f5";
            brightnessdown = "f6";
            brightnessup = "f7";
            kbdillumtoggle = "f8";
            playpause = "f9";
            mute = "f10";
            volumedown = "f11";
            volumeup = "f12";
          };
          meta = {
            f1 = "f1";
            f2 = "f2";
            f3 = "f3";
            f4 = "f4";
            f5 = "f5";
            f6 = "f6";
            f7 = "f7";
            f8 = "f8";
            f9 = "f9";
            f10 = "f10";
            f11 = "f11";
            f12 = "f12";
            up = "pageup";
            down = "pagedown";
            left = "back";
            right = "forward";
            backspace = "delete";
          };
        };
      };
    };
    openssh.enable = true;
    pcscd.enable = true;
    pipewire = {
      enable = true;
      alsa.enable = true;
      pulse.enable = true;
    };
    printing.enable = false;
    resolved = {
      llmnr = "false";
    };
    tailscale = { 
      enable = false;
      useRoutingFeatures = "client";
    };
    auto-cpufreq.enable = true;
    thermald.enable = true;
    tlp.enable = lib.mkForce false;
  };

  powerManagement.powertop.enable = true;

  programs = {
    adb.enable = true;
    htop = {
      enable = true;
      settings = {
        hide_kernel_threads=0;
        hide_userland_threads=1;
        show_program_path=0;
        tree_view=1;
      };
    };
    firefox.enable = true;
    mtr.enable = true;
    nano.enable = false;
    nix-ld.enable = true;
    neovim = {
      enable = true;
      viAlias = true;
      vimAlias = true;
      configure = {
        customRC = ''
          set backupdir=~/.vim/backups
        '';
        packages.myPlugins = with pkgs.vimPlugins; {
          start = [ "vim-nix" ];
        };
      };
    };
    sway.enable = true;
  };

  hardware = {
    bluetooth.enable = true;
    bluetooth.powerOnBoot = true;
    #enableAllFirmware = true;
    enableRedistributableFirmware = true;
  };

  #sound.enable = true;
  services.blueman.enable = true;

  environment.etc = {
    "libinput/local-overrides.quirks".text = ''
      [Google Chromebook Banshee]
      MatchUdevType=touchpad
      MatchName=PNP0C50:00 093A:0274 Touchpad
      MatchDMIModalias=dmi:*svnGoogle:*pnBanshee*
      ModelChromebook=1
      AttrPressureRange=6:4
    '';
    "wireplumber/bluetooth.lua.d/51-bluez-config.lua".text = ''
      bluez_monitor.properties = {
        ["bluez5.enable-sbc-xq"] = true,
        ["bluez5.enable-msbc"] = true,
        ["bluez5.enable-hw-volume"] = true,
        ["bluez5.headset-roles"] = "[ hsp_hs hsp_ag hfp_hf hfp_ag ]"
      }
    '';
  };

  security = { 
    sudo-rs = {
      enable = true;
      wheelNeedsPassword = false;
      execWheelOnly = true;
    };
    rtkit.enable = true;
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It's perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.11"; # Did you read the comment?
}
