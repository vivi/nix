{
  disko.devices = {
    disk = {
      nvme = {
        type = "disk";
        device = "/dev/disk/by-id/nvme-eui.e8238fa6bf530001001b448b4b3750aa";
        content = {
          type = "gpt";
          partitions = {
            ESP = {
              size = "512M";
              type = "EF00";
              label = "boot";
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot";
                mountOptions = [ "fmask=0077" "dmask=0077" "defaults" ];
              };
            };
            luks = {
              size = "100%";
              content = {
                type = "luks";
                name = "crypted";
                passwordFile = "/tmp/secret.key";
                settings = {
                  allowDiscards = true;
                };
                content = {
                  type = "btrfs";
                  extraArgs = [ "-f" ];
                  subvolumes = {
                    "/nix" = {
                      mountpoint = "/nix";
                      mountOptions = [ "defaults" "compress=zstd" "noatime" ];
                    };
                    "/persist" = {
                      mountpoint = "/persist";
                      mountOptions = [ "defaults" "compress=zstd" "noatime" ];
                    };
                    "/nixos-config" = {
                      mountpoint = "/etc/nixos";
                      mountOptions = [ "defaults" "compress=zstd" "noatime" ];
                    };
                  };
                };
              };
            };
          };
        };
      };
    };
    nodev = {
      "/" = {
        fsType = "tmpfs";
        mountOptions = [
          "defaults"
          "mode=755"
        ];
      };
    };
  };
  fileSystems."/persist" = {
    neededForBoot = true;
  };
  services = {
    btrfs.autoScrub = {
      enable = false;
      interval = "weekly";
    };
    fstrim.enable = true;
  };
  #system.activationScripts = {
  #  fix-var-run-bad.text = ''
  #    mkdir -p /var
  #    ln -sf /var/run /run
  #  '';
  #};
}
