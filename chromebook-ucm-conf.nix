{ lib, stdenv, fetchFromGitHub }:

stdenv.mkDerivation rec {
  pname = "chromebook-ucm-conf";
  version = "0.1";

  src = fetchFromGitHub {
    owner = "WeirdTreeThing";
    repo = "chromebook-ucm-conf";
    rev = "6d8835f89c7c7e5fac7b6aaea0e36a6b1d1e4be5";
    hash = "sha256-XQ/kRN1xHEAcykbKpE26mNZXwqEbkh++ZuJeCAiu9ho=";
  };

  dontBuild = true;

  installPhase = ''
    runHook preInstall
  
    #mkdir -p $out/share/alsa
    #cp -r ucm ucm2 $out/share/alsa
  
    mkdir -p $out/share/alsa/ucm2/conf.d
    cp -r common/pcm adl/* $out/share/alsa/ucm2/conf.d
  
    runHook postInstall
  '';

  meta = with lib; {
    homepage = "https://www.alsa-project.org/";
    description = "ALSA Use Case Manager configuration";

    longDescription = ''
      The Advanced Linux Sound Architecture (ALSA) provides audio and
      MIDI functionality to the Linux-based operating system.
    '';

    license = licenses.bsd3;
    maintainers = [ maintainers.vivithecanine ];
    platforms = platforms.linux;
  };
}
