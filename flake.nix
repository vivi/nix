{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";
    disko.url = "github:nix-community/disko";
    impermanence.url = "github:nix-community/impermanence";
  };

  outputs = { nixpkgs, nixos-hardware, disko, impermanence, ... }: {
    nixosConfigurations = {
      "puppy" = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          disko.nixosModules.disko
          impermanence.nixosModules.impermanence
          nixos-hardware.nixosModules.common-cpu-intel
          #nixos-hardware.nixosModules.common-pc-laptop-ssd
          ./configuration.nix
        ];
      };
    };
  };
}
